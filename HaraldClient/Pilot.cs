﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

public interface IPilot
{
    void MoveForward();
    void Travel(float distance);
    void StopMoving();
    string GetSpeed();
    void SetSpeed(float speed);
    void Rotate(double degrees);
}

public class Pilot : IPilot
{
    private Robot robot;

    public Pilot(Robot robot)
    {
        this.robot = robot;
    }

    public void MoveForward()
    {
        robot.SendCommand("differentialpilot/forward");
    }

    public void Travel(float distance)
    {
        robot.SendCommand("/differentialpilot/run/" + distance);
    }

    public void StopMoving()
    {
        robot.SendCommand("differentialpilot/stop");
    }

    public string GetSpeed()
    {
        string speed = robot.SendCommand("differentialpilot/getmovementincrement");
        return speed;
    }

    public void Rotate(double degrees)
    {
        robot.SendCommand("differentialpilot/rotate/" + degrees);
    }

    public void SetSpeed(float speed)
    {
        robot.SendCommand("differentialPilot/setspeed/" + speed);
    }
}
