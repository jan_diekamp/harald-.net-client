﻿using System;
using System.Threading;

namespace HaraldClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Robot robot = new Robot();
            Pilot pilot = new Pilot(robot);
            SampleProviderForColorSensor sampleProvider = new SampleProviderForColorSensor(robot);
            Sound sound = new Sound(robot);

            sound.MakeBeep();

            Color color = sampleProvider.IdentifyColor();
            Console.WriteLine(color);
            //var name = Console.ReadLine();

            pilot.Rotate(10.0);

            pilot.MoveForward();
            Thread.Sleep(500);
            pilot.StopMoving();

            sound.MakeBuzz();
        }
    }
}
