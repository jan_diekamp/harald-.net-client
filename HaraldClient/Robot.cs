﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

public interface IRobot
{
    string SendCommand(string command);
}

public class Robot
{
    private readonly string ip = "http://192.168.0.102:8080/"; 
    public HttpResponseMessage response;
    public HttpClient client;

    public Robot()
    {
        client = new HttpClient();
        SetClientProperties(client);
    }

    private void SetClientProperties(HttpClient client)
    {
        client.BaseAddress = new Uri(ip);
        client.DefaultRequestHeaders.Accept.Clear();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
    }

    public string SendCommand(string command)
    {
        string answer;

        answer = client.GetAsync(command).Result.Content.ReadAsStringAsync().Result;

        return answer;
    } 
}
