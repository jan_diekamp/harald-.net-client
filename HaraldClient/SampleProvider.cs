﻿using System;


public enum Color
{
    BLACK = 7,
    BLUE = 2,
    CYAN = 12,
    DARK_GRAY = 11,
    GRAY = 9,
    GREEN = 1,
    LIGHT_GRAY = 10,
    MAGENTA = 4,
    NONE = -1,
    ORANGE = 5,
    PINK = 8,
    RED = 0,
    WHITE = 6,
    YELLOW = 3
}

public interface IColorSampleProvider
{
    Color IdentifyColor();
    void IdentifyColor(out Color outParameter);
}

public interface IInfraredSampleProvider
{
    int GetDistance();
    void GetDistance(out int outParameter);
}

public class SampleProviderForColorSensor : IColorSampleProvider
{
    private Robot robot;

    public SampleProviderForColorSensor(Robot robot)
    {
        this.robot = robot;
    }

    public Color IdentifyColor()
    {
        string color = robot.SendCommand("color/getcolor");
        return (Color)Int32.Parse(color);
    }

    public void IdentifyColor(out Color outParameter)
    {
        string color = robot.SendCommand("color/getcolor");
        outParameter = (Color)Int32.Parse(color);
    }
}

public class SampleProviderForInfraredSensor : IInfraredSampleProvider
{
    private Robot robot;

    public SampleProviderForInfraredSensor(Robot robot)
    {
        this.robot = robot;
    }

    public int GetDistance()
    {
        string distance = robot.SendCommand("distance/getdistance");
        return Int32.Parse(distance);
    }

    public void GetDistance(out int outParameter)
    {
        string distance = robot.SendCommand("distance/getdistance");
        outParameter = Int32.Parse(distance);
    }
}
