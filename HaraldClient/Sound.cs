﻿public interface ISound
{
    void MakeBeep();
    void MakeBuzz();
}

public class Sound : ISound
{
    private Robot robot;

    public Sound(Robot robot)
    {
        this.robot = robot;
    }

    public void MakeBeep()
    {
        robot.SendCommand("sound/beep");
    }

    public void MakeBuzz()
    {
        robot.SendCommand("sound/buzz");
    }
}
